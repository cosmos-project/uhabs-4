# UH ME419 Astronautics Project: UHABS-4 #

This repository contains all the software for the UHABS4 project. It is based on [COSMOS](https://www.cosmos-project.org)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner: eshay@hawaii.edu
